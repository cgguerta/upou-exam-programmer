function lovecalc(array) {
		var hold = [],
				result,
				newArray;
		if (array.length > 2) {
				newArray = array.map(function(item, index, array) {
						return item + array[index + 1];
				});
				newArray.forEach(function(item) {
						if (typeof item === "number") {
								if (item <= 9) {
										hold.push(item);
								} else if (item >= 10) {
										console.log('Splitting' + item + " into " + item.toString()[0] + " and " + item.toString()[1]);
										hold.push(parseInt(item.toString()[0]));
										hold.push(parseInt(item.toString()[1]));
								}
						} else {
								console.log("failed number check: " + item);
						}
				});
				lovecalc(hold);
		} else {
				document.getElementById('result').textContent = "you are" + " " + array[0] + "" + array[1] + "%" + " " + "compatible!";
				result = array[0] + "" + array[1] + "%";
				return result;
		}
}

function calculate() {
		var inputs = document.getElementsByName('names'),
				trueLove = ["t", "r", "u", "e", "l", "o", "v"],
				countArray = [],
				count,
				names,
				bothNames;
		document.getElementById('warning').textContent = "";
		if (!inputs[0].value || !inputs[1].value) {
				document.getElementById('warning').textContent = "Please enter both names.";
		} else {
				names = "" + inputs[0].value + "" + inputs[1].value + "";
				bothNames = names.toLowerCase();
				countArray = trueLove.map(function(item) {
						count = 0;
						for (var i = 0; i < bothNames.length; i += 1) {
								if (item === bothNames[i]) {
										count += 1;
								}
						}
						return count;
				});
				lovecalc(countArray);
				console.log(countArray)
				console.log(names)
				console.log(count)
				console.log(bothNames.length)
		}
}

document.getElementById('submit').addEventListener('click', function(e) {
		e.preventDefault();
		calculate();
}, false);